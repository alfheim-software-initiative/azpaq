use std::process::Command;
use std::str;

pub fn compress(dest: &str, src: &str) {
    let cz = Command::new("zpaq")
        .arg("add")
        .arg(&dest)
        .arg(&src)
        .arg("-m")
        .arg("5")
        .output()
        .expect("Failed to compress file!");
    cz.stdout;
}
