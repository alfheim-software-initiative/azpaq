use std::process::Command;
use std::str;

pub fn extract(src: &str, dest: &str) {
    let ez = Command::new("zpaq")
        .arg("extract")
        .arg(&src)
        .arg(&dest)
        .output()
        .expect("Failed to extract file!");
    ez.stdout;
}
